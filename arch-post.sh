ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime

sed --in-place=.bak 's/^#en_US\.UTF-8/en_US\.UTF-8/' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf

echo arch-pc > /etc/hostname

# bootloader
grub-install --target=x86_64-efi --bootloader-id=grub_uefi
grub-mkconfig -o /boot/grub/grub.cfg

# sudo
echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers


sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
pacman -Syu --noconfirm


pacman -S --noconfirm xorg-server
pacman -S --noconfirm xorg-drivers
pacman -S --noconfirm plasma
pacman -S --noconfirm ark
pacman -S --noconfirm dolphin
pacman -S --noconfirm dolphin-plugins
pacman -S --noconfirm ffmpegthumbs
pacman -S --noconfirm gwenview
pacman -S --noconfirm kdeconnect
pacman -S --noconfirm sddm
pacman -S --noconfirm pipewire
pacman -S --noconfirm lib32-pipewire
pacman -S --noconfirm lib32-pipewire-jack
pacman -S --noconfirm pipewire-alsa
pacman -S --noconfirm pipewire-jack
pacman -S --noconfirm pipewire-pulse
pacman -S --noconfirm wireplumber
pacman -S --noconfirm mpv
pacman -S --noconfirm firefox
pacman -S --noconfirm networkmanager

echo "starting service"
systemctl enable NetworkManager.service


echo "Setting root password"
passwd


echo "Setting user password"
useradd -m -G wheel -s /bin/bash hacker47
passwd hacker47
