parted -s ${1} mklabel gpt
parted -s ${1} mkpart "EFI" fat32 1MIB 512MIB
parted -s ${1} mkpart "swap" linux-swap 512MIB 1GIB
parted -s ${1} mkpart "root" ext4 1024MIB 100%
parted -s ${1} set 1 esp on

mkswap ${1}2
swapon ${1}2

pacman -Syy

mount ${1}3 /mnt
mkdir /mnt/boot/efi
mount ${1}1 /mnt/boot/efi

pacstrap /mnt base linux-zen linux-firmware grub efibootmgr os-prober mtools sudo vim

genfstab -U /mnt >> /mnt/etc/fstab

echo Executing post-init script
curl https://gitlab.com/traplover420/arch_installer/-/raw/main/arch-post.sh > /mnt/root/arch-post.sh
arch-chroot /mnt "/bin/bash" "/root/arch-post.sh"


umount /mnt
